#!/usr/bin/python3

import queue
import threading

from logger import Logger
from sc16is740 import SC16IS740
from nSentencer import NmeaSentencer

class ServerThread(threading.Thread,Logger):
    """
    Implent the serving of the SC16IS740 
    """
    def __init__(self, mutex, dataOutQ, stopEvent):
        threading.Thread.__init__(self,daemon=True)
        Logger.__init__(self,mutex,'SC16IS740 Server')
        self.outQ        = dataOutQ
        self.stopEvent   = stopEvent
        self.sc          = SC16IS740()
        self.logMessage('ServerThread instance created')

    def run(self):
        try:
            while not self.stopEvent.is_set():  # monitor a stop event... more explanations later
                bytes_to_read = self.sc.dataAvailable()  # blocking call
                while (bytes_to_read > 0):
                    self.outQ.put(self.sc.getDataBlock())
                    bytes_to_read = self.sc.dataAvailable(False) # non blocking call
            #q.join()
        except Exception as e:
            print(e)
            self.logMessage('* Server thread exiting on exception:' + str(e))
            
class DataProcessorThread(threading.Thread,Logger):
    """
    Implent the processing of data 
    """
    def __init__(self,mutex, dataInQ, stopEvent):
        threading.Thread.__init__(self,daemon=True)
        Logger.__init__(self,mutex,'Data Processor Thread')
        self.inQ        = dataInQ
        self.stopEvent  = stopEvent
        self.ns         = NmeaSentencer()
        self.it         = iter(self.ns)
        self.logMessage('DataProcessorThread instance created')
        
    def processIncoming(self,item):
        # msg = 'processing: <start>' + str(item) + '<end> from inQ'
        # self.logMessage(msg)
        self.ns.parseIncoming(item)
        try:
            self.logMessage(next(self.it))
        except IndexError:
            return
        
    def run(self):
        print('processor running..')
        while not (self.stopEvent.is_set() and self.inQ.empty()):
            partialMsg =  self.inQ.get() # blocking synchronized call, if q empty, we blaock
            self.inQ.task_done()
            self.processIncoming(partialMsg)

def startup():
    stopEvent = threading.Event()
    stopEvent.clear()
    Q = queue.Queue()
    mutex = threading.Lock()
    server = ServerThread(mutex,Q,stopEvent)
    processor = DataProcessorThread(mutex,Q,stopEvent)
    processor.start()
    server.start()
    while True:
        try:
            ## these calls block until the threads exit
            server.join()
            processor.join()
        except KeyboardInterrupt: # or anything else to stop
            print('\nSetting stop event and waiting on threads')
            stopEvent.set()

if __name__ == '__main__':
    print('Starting up...')
    startup()

           
