#!/usr/bin/python3

from time import strftime
import os

class Logger():
    filename = strftime('%Y_%m_%d_%H_%M.log')
    init     = False
    def __init__(self, mutex,name):
        self.mutex = mutex
        self.name = name
        self.mutex.acquire()
        try:
            if not Logger.init:
                with open(Logger.filename, 'a') as f:
                    msg = 'Logging from: ' + os.getcwd()
                    f.write(msg +'\n')
                Logger.init = True
        finally:
            self.mutex.release()

        
    def logMessage(self,msg):
         self.mutex.acquire()
         try:
             logText = self.name +': ' + msg
             print(logText)
             with open(Logger.filename, 'a') as f:
                 f.write(logText+'\n')
         finally:
             self.mutex.release()
    

