#!/usr/bin/python3

import RPi._GPIO as GPIO # for interrupts

int_pin         = 12

def cb(channel):
    print ('cb: ', channel)

def start():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(int_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.remove_event_detect(int_pin)
    GPIO.add_event_detect(int_pin, GPIO.FALLING, callback=cb)

if __name__ == "__main__":
    try:
        start()
        while True:
            pass
    except Exception  as e:
        print(e)
    finally:
        GPIO.cleanup()