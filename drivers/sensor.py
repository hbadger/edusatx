class Sensor:
    """ Abstract interface class for a generic sensor. """

    def __init__(self, name):
        self.measuresDict   = {}
        self.name           = name

    def whatHaveIgot(self):
        return self.measuresDict.keys()

    def giveItToMe(self, what):
        return self.measuresDict[what]()
    
    def whoAmI(self):
        return self.name

