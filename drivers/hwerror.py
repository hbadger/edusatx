class HardwareError(IOError):
    """ Represents a hardware error in a sensor or actuator of the satellite. """

    def __str__(self):
        return 'Hardware error.'