#!/usr/bin/python3

from atgm336h import ATGM336H
import smbus2
from time import sleep






class Sensors:
    """ Interface class for satellite sensors. Automatically searches for attached sensors when initialised. """

    # I2C address list for detection
    gnss_module     = 0x4C      # SC16IS740 + ATGM336H-5N
    module_dict     = {0x4C: ATGM336H}

    def __init__(self):
        """ Initialisation of the satellite's sensor array. """

        # initialise the I2C bus through Python's SMBus interface and search for attached devices:
        addr_list = self._i2c_detect()

        

        #self.GNSSAvailable = False
        self.sensorDict = {}

        for addr in addr_list:
            try:
                cl = Sensors.module_dict[addr]
                inst = cl()
                self.sensorDict[inst.whoAmI()] = inst
                #print ('GNSS module found and initialised.')
                #self.GNSSAvailable = True
            except:
                pass

    
    def _i2c_detect(self):
        """ Attempts to read a byte from every I2C address to check if a device is responding on that address. Returns a list of active addresses. """

        bus = smbus2.SMBus(1)
        addr_list = []

        for addr in range(0x03, 0x77):
            try:
                bus.read_byte(addr)
                addr_list.append(addr)
            except:
                pass
        
        return addr_list

    def getAvailableSensors(self):
        """ Returns a list of names of all available sensors. """
        return list(self.sensorDict.keys())

    def getSensor(self, sensorName):
        """ Returns a sensor instance for a sensor with a specified name. """
        return self.sensorDict[sensorName]

    def getWhatItsGot(self, sensorName):
        """ Returns a description of a sensor with a specified name. """
        return list(self.getSensor(sensorName).whatHaveIgot())

    def getSensorValue(self, sensorName, measureName):
        """ Returns the measurement corresponding to the specified measure name for a sensor. """
        return self.getSensor(sensorName).giveItToMe(measureName)
    


if __name__ == "__main__":
    
    # search for sensors and initialise them:
    sensors = Sensors()

    while True:

        for sensorname in sensors.getAvailableSensors():
            print('Sensor Available: ', sensorname)
            print('Sensor Measurements: ', sensors.getWhatItsGot(sensorname))
            for what in sensors.getWhatItsGot(sensorname):
                print(what, sensors.getSensorValue(sensorname, what))


        #sleep(1)
        print('\n')

