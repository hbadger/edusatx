#!/usr/bin/python3

import smbus2 as smbus

PCF8593_default_addr = 0b1010001    # no address select bits on this device, see § 7.2.1 in datasheet

reg_Ctrl_Stat      = 0x00
reg_h_seconds      = 0x01
reg_seconds        = 0x02
reg_minutes        = 0x03
reg_hours          = 0x04
reg_years_date     = 0x05
reg_days_months    = 0x06
reg_timer          = 0x07
reg_alarm_ctrl     = 0x08
reg_self_test      = 0x0F

# bit encoding
bit7 = 0x80
bit6 = 0x40
bit5 = 0x20
bit4 = 0x10
bit3 = 0x08
bit2 = 0x04
bit1 = 0x02
bit0 = 0x01

Ctrl_timer_flag         = bit0
Ctrl_alarm_flag         = bit1
Ctrl_alarm_en           = bit2
Ctrl_mask_flag          = bit3
Ctrl_32k_clock_mode     = 0
Ctrl_50Hz_clock_mode    = bit4
Ctrl_event_counter_mode = bit5
Ctrl_test_mode          = bit4 | bit5
Ctrl_hold_last_count    = bit6
Ctrl_stop_counting      = bit7

Hours_hours_BCD         = bit0 | bit1 | bit2 | bit3
Hours_tens              = bit4 | bit5
Hours_AM_PM             = bit6
Hours_24h_format        = 0
Hours_12h_format        = bit7

Years_days_BCD          = bit0 | bit1 | bit2 | bit3
Years_tens              = bit4 | bit5
Years_year              = bit6 | bit7

Days_months_BCD         = bit0 | bit1 | bit2 | bit3
Days_tens               = bit4
Days_weekdays           = bit5 | bit6 | bit7

Alarm_no_timer          = 0
Alarm_hudreds_sec       = bit0
Alarm_seconds           = bit1
Alarm_minutes           = bit0 | bit1
Alarm_hours             = bit2
Alarm_days              = bit0 | bit2
Alarm_not_used          = bit1 | bit2
Alarm_test_mode         = bit1 | bit2 | bit3
Alarm_int_flag          = bit3
Alarm_no_clock_alarm    = 0
Alarm_daily             = bit4
Alarm_weekdaily         = bit5
Alarm_dated             = bit4 | bit5
Alarm_timer_alarm       = bit6
Alarm_interrupt         = bit7

Event_no_timer          = 0
Event_units             = bit0
Event_100               = bit1
Event_10k               = bit0 | bit1
Event_1M                = bit2
Event_test_mode         = bit0 | bit1 | bit2
Event_int_enable        = bit3
Event_no_event_alarm    = 0
Event_alarm             = bit4
Event_timer_alarm_en    = bit6
Event_alarm_int_en      = bit7




class PCF8593:
    """ Device driver for PCF8593 timer/counter/clock. """

    def __init__(self, addr, intPin):
        self.bus = smbus.SMBus(1)       # connect to I2C bus on GPIO pins
        self._pcf8593_addr = addr
        self._intPin = intPin


        

    def _selfTest(self):
        """ Test device connectivity by writing a magic value to one of the RAM addresses and reading it back. """
        magic_value = 0x42

        self._writeByte(reg_self_test, magic_value)     # write 0x42 (66 dec) to the test register
        return_value = self._readByte(reg_self_test)

        if (return_value == magic_value):
            return True
        else:
            print('Self test failed, returned value was ' + str(return_value))
            return False

    def _writeByte(self, reg, value):
        """ Write a single byte to the specified register """
        self.bus.write_byte_data(self._pcf8593_addr, reg, value)

    def _readByte(self, reg):
        """ Read a single byte from the specified register. """
        return self.bus.read_byte_data(self._pcf8593_addr, reg)


